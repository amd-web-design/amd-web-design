import { Redirect, Route, Switch } from 'react-router-dom';
import AboutUs from './views/AboutUs';
import Welcome from './views/Welcome';
import MainHeader from './components/MainHeader';

function App() {
  return (
      <div>
        <MainHeader />
        <main>
          <Switch>
            <Route path="/" exact>
              <Redirect to='/welcome' /> 
            </Route>
            <Route path="/welcome" exact>
              <Welcome /> 
            </Route>
            <Route path="/about-us" exact>
              <AboutUs /> 
            </Route>
            <Route>
              <h1>Default 404</h1>
            </Route>
          </Switch>
        </main>
      </div>
  );
}

export default App;
