import { NavLink } from 'react-router-dom';

import classes from './index.module.css';

const MainHeader = () => {
    return (
        <header className={classes.header}>
            <nav>
                <ul>
                    <li>
                        <NavLink activeClassName={classes.active} to='/welcome'>
                            Welcome
                        </NavLink>
                    </li>
                    <li>
                        <NavLink activeClassName={classes.active} to='/about-us'>
                            About Us
                        </NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    )
}

export default MainHeader;