import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './App';

// const basename = window.location.hostname.includes('gitlab') ? 'amd-web-design/#' : '#'
if (!window.location.pathname.endsWith('/')) {
    window.location.pathname = window.location.pathname + '/'
}
// If its at localhost, then 
if (window.location.hostname.includes('localhost') && window.location.pathname.startsWith('/amd-web-design') && !window.location.pathname.startsWith('/amd-web-design/')) {
    console.log(window.location.pathname);
    console.log(window.location.hash);
    window.location.pathname = '/amd-web-design/'
}


const basename = window.location.hostname.includes('gitlab') ? 'amd-web-design/' : ''


const index = basename ? 1 : 3;
let path_starts_with_hash = ''

path_starts_with_hash = window.location.href.split('/' + basename)[index].startsWith('#')

if (!path_starts_with_hash ) {
    const newHrefArray = window.location.href.split('/'+ basename)
    newHrefArray.splice(index, 0, basename + '#')
    window.location.href = newHrefArray.join('/')
}

ReactDOM.render(<Router basename={basename + '#'}><App /></Router>, document.getElementById('root'));
