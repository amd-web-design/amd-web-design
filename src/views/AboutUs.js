import UnderConstruction from "../components/UnderConstruction";

const AboutUs = () => {
    return (
        <section>
            <h1>About Us</h1>
            <UnderConstruction />
        </section>
    );
};

export default AboutUs;