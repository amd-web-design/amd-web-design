import UnderConstruction from "../components/UnderConstruction"

const Welcome = () => {
    return (
        <section>
            <h1>Welcome</h1>
            <UnderConstruction />
        </section>
    );
};

export default Welcome;